const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();
app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//inicialitzem mongoose
const db = require("mongoose");
db.Promise = global.Promise;

const conn_url = "mongodb://localhost:27017/contactes_db";

//connectem a la bdd
db.connect(conn_url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(() => {
        console.log("Connexió OK!");
    })
    .catch(err => {
        console.log("Connexió falla...", err);
        process.exit();
    });

db.set('useFindAndModify', false);

//creem esquema i model 'Contacte'
const contacteCollection = db.Schema({ nom: String, email: String, telefon: String });
const Contacte = db.model('contacte', contacteCollection);


// definim rutes de la API
app.get("/", (req, res) => {
    res.send("<h1>Benvingut a la API de Contactes!</h1>");
});

// consulta TOTS els contactes
app.get("/api/contactes", (req, res) => {
    Contacte.find()
        .then(data => {
            // hacemos cosas con los datos antes de enviar...
            res.status(200).json(data);
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});


// consulta UN contacte per ID
app.get("/api/contactes/:id", (req, res) => {
    const id = req.params.id;
    Contacte.findById(id)
        .then(data => {
            res.status(200).json(data);
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});


// crea un contacte
app.post("/api/contactes", (req, res) => {
    if (!req.body.nom) {
        res.status(400).send({ error: "No trobo el contacte!" });
        return;
    }

    // contacte nou
    const contacte = new Contacte({
        nom: req.body.nom,
        email: req.body.email,
        telefon: req.body.telefon,
    });

    // el guardem a la bdd

    contacte.save(contacte)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});


// modifica un contacte
app.put("/api/contactes/:id", (req, res) => {
    if (!req.body) {
        res.status(400).send({ error: "No trobo dades!" });
        return;
    }

    const id = req.params.id;

    Contacte.findByIdAndUpdate(id, req.body)
        .then(data => {
            if (!data) {
                res.send({error: "No s'ha actualitzat res"});
            } else {
                res.send({msg: "Actualitzat"});
            }
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});

//elimina un contacte
app.delete("/api/contactes/:id", (req, res) => {
    const id = req.params.id;
    Contacte.findByIdAndRemove(id)
        .then(data => {
            if (!data) {
                res.status(500).send({error: "No s'ha eliminat res"});
            } else {
                res.send({msg: "Eliminat", id: id });
            }
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});




// set port, listen for requests
const PORT = 8080;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});
